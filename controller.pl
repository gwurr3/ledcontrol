#!/usr/bin/perl
#
#
# this will be the controller daemon for the serial-connected microcontoller.... eventually


use strict;
use warnings;
$| = 1;

local $/ = "\r" ; # set line ending to CR
#local $/ = "\r\n" ; # set line ending to CRLF
#
#
sub sendCommand
{
	my $toModem = $_[0] . "\015";

	print MODEM_OUT $toModem ;
	while ( <MODEM_IN> ) {
	     last if /OK/; # look for "OK" and break if found
	    chomp;
	    print STDERR $_ ;
	}


}


my $modem_device = "/dev/cua00";

use IPC::Open2;
#my $stty = `/bin/stty -g`;
open2( \*MODEM_IN, \*MODEM_OUT, "cu -l$modem_device ");
# starting cu hoses /dev/tty's stty settings, even when it has
# been opened on a pipe...
#system("/bin/stty $stty");



my $line;
while ( 1 ) {

	sendCommand("SD1");
	sleep(.8);
	sendCommand("SD2");
	sleep(.8);
	sendCommand("SD3");
	sleep(.8);
	sendCommand("SD4");
	sleep(.8);
	sendCommand("SD5");
	sleep(.8);
	sendCommand("SD6");
	sleep(.8);
	sendCommand("SD7");
	sleep(.8);
	sendCommand("SD8");
	sleep(.8);
	sendCommand("SD9");
	sleep(.8);
	sendCommand("SD0");
	sleep(.8);
	sendCommand("SDo");
	sleep(.8);

}

