#include <avr/io.h>
#include "outputs.h"
#include "PCA9555.h"
#include <util/delay.h>


// Some macros that make the code more readable
#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)


#define BITNULL 0b00000000;
#define BIT0 0b00000001;
#define BIT1 0b00000010;
#define BIT2 0b00000100;
#define BIT3 0b00001000;
#define BIT4 0b00010000;
#define BIT5 0b00100000;
#define BIT6 0b01000000;
#define BIT7 0b10000000;
#define BIT255 0b11111111;

uint8_t buffer0 = BITNULL ;

void initPins() {
//	DDRC = 0b11111111; // ALL PINS AS OUTPUTS
//	PORTC = 0b00000000; // ALL PINS OFF


	// PWM STUFF
	// Port D6 as output
	DDRD   |= (1 << 6);

	TCCR0A |= (1 << COM0A1);
	// set none-inverting mode
	
	TCCR0A |= (1 << WGM01) | (1 << WGM00);
	// set fast PWM Mode
	
	TCCR0B |= (1 << CS01);
	// set prescaler to 8 and starts PWM

	SetPWMOutput('o');


	//uint8_t pca9555interrupt = 0; // global PCA9555 INT detector
	//SIGNAL(INT0_vect) { pca9555interrupt = 1; } // PCA9555 INT changed
	PCA9555_init(); // start up

	PCA9555_dir(PCA9555_DEV_000, PCA9555_PORT_0, 0b00000000); // port 0 output
	PCA9555_dir(PCA9555_DEV_000, PCA9555_PORT_1, 0b00000000); // port 1 output
}

void testPin() {

	uint8_t pca9555val = 0;

	while( pca9555val < 255 ) {
		PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, pca9555val++); 
		_delay_ms(50);
	}
	_delay_ms(1000);
	PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0); 
}



void SetPWMOutput(char input)
{

     	switch( input )
	{
	case 'o' : OCR0A = 0; ; break;
	case '1' : OCR0A = 15; ; break;
	case '2' : OCR0A = 31; ; break;
	case '3' : OCR0A = 56; ; break;
	case '4' : OCR0A = 102; ; break;
	case '5' : OCR0A = 127; ; break;
	case '6' : OCR0A = 153; ; break;
	case '7' : OCR0A = 178; ; break;
	case '8' : OCR0A = 204; ; break;
	case '9' : OCR0A = 229; ; break;
	case '0' : OCR0A = 255 ; break; 
	}

}


void pinOn(int i) {
//
//	switch( i )
//	{
//	case 0 : buffer0 |= BIT0 ; break; 
//	case 1 : buffer0 |= BIT1 ; break;
//	case 2 : buffer0 |= BIT2 ; break;
//	case 3 : buffer0 |= BIT3 ; break;
//	case 4 : buffer0 |= BIT4 ; break;
//	case 5 : buffer0 |= BIT5 ; break;
//	case 6 : buffer0 |= BIT6 ; break;
//	case 7 : buffer0 |= BIT7 ; break;
//	}
//
	//PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, buffer0); 
	//
	switch( i )
	{
	case 0 : PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0b00000001 ); break; 
	case 1 : PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0b00000010 ); break;
	case 2 : PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0b00000100 ); break;
	case 3 : PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0b00001000 ); break;
	case 4 : PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0b00010000 ); break;
	case 5 : PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0b00100000 ); break;
	case 6 : PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0b01000000 ); break;
	case 7 : PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, 0b10000000 ); break;
	}



}



void pinOff(int i) {

	switch( i )
	{
	case 0 : buffer0 &= ~BIT0 ; break; 
	case 1 : buffer0 &= ~BIT1 ; break;
	case 2 : buffer0 &= ~BIT2 ; break;
	case 3 : buffer0 &= ~BIT3 ; break;
	case 4 : buffer0 &= ~BIT4 ; break;
	case 5 : buffer0 &= ~BIT5 ; break;
	case 6 : buffer0 &= ~BIT6 ; break;
	case 7 : buffer0 &= ~BIT7 ; break;
	}

	PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, buffer0); 

}



void pinToggle(int i) {

	switch( i )
	{
	case 0 : buffer0 ^= BIT0 ; break; 
	case 1 : buffer0 ^= BIT1 ; break;
	case 2 : buffer0 ^= BIT2 ; break;
	case 3 : buffer0 ^= BIT3 ; break;
	case 4 : buffer0 ^= BIT4 ; break;
	case 5 : buffer0 ^= BIT5 ; break;
	case 6 : buffer0 ^= BIT6 ; break;
	case 7 : buffer0 ^= BIT7 ; break;
	}

	PCA9555_set(PCA9555_DEV_000, PCA9555_PORT_0, buffer0); 

}

