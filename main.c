
#define UART_BAUD_RATE 9600 //uart speed

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <string.h>
#include "uart.h"
#include "outputs.h"

#include "main.h"

#define TRUE 1
#define FALSE 0
#define CHAR_NEWLINE '\n'
#define CHAR_RETURN '\r'
#define RETURN_NEWLINE "\r\n"

unsigned char data_count = 0;
unsigned char data_in[8];
char command_in[8];
int variable_A = 23; //user modifiable variable
int variable_goto = 12; //user modifiable variable

int parse_assignment (char input[16]) {
  char *pch;
  char cmdValue[16];
  // Find the position the equals sign is
  // in the string, keep a pointer to it
  pch = strchr(input, '=');
  // Copy everything after that point into
  // the buffer variable
  strcpy(cmdValue, pch+1);
  // Now turn this value into an integer and
  // return it to the caller.
  return atoi(cmdValue);
}

void copy_command () {
  // Copy the contents of data_in into command_in
  memcpy(command_in, data_in, 8);
  // Now clear data_in, the UART can reuse it now
  memset(data_in, 0, 8);
}

void process_command() {
/*
  if(strcasestr(command_in,"GOTO") != NULL){
    if(strcasestr(command_in,"?") != NULL)
      print_value("goto", variable_goto);
    else
      variable_goto = parse_assignment(command_in);
  }
  else if(strcasestr(command_in,"A") != NULL){
    if(strcasestr(command_in,"?") != NULL)
      print_value("A", variable_A);
    else
      variable_A = parse_assignment(command_in);
  }
*/
   switch (command_in[0]) {
        case 'S':
	    if (command_in[1] == 'H') {
	    	int pinval = command_in[2] - '0';
	    	if ( pinval >= 0 && pinval <= 7) {
	    	    	pinOn(pinval);
			uart_puts("HIGH\r\n");
	    	}
	    } else if (command_in[1] == 'L') {
	    	int pinval = command_in[2] - '0';
	    	if ( pinval >= 0 && pinval <= 7) {
	    	    	pinOff(pinval);
			uart_puts("LOW\r\n");
	    	}

	    } else if (command_in[1] == 'T') {
	    	int pinval = command_in[2] - '0';
	    	if ( pinval >= 0 && pinval <= 7) {
	    	    	pinToggle(pinval);
			uart_puts("TOGGLE\r\n");
	    	}

	    } else if (command_in[1] == 'D') {
  		SetPWMOutput(command_in[2]);
	    }
            break;
        default:
            uart_puts("NOT RECOGNISED\r\n");
            break;
    }

} 

void print_value (char *id, int value) {
  char buffer[8];
  itoa(value, buffer, 10);
  uart_puts(id);
  uart_putc('=');
  uart_puts(buffer);
  uart_puts(RETURN_NEWLINE);
}

void uart_ok() {
  uart_puts("OK");
  uart_puts(RETURN_NEWLINE);
}

void process_uart(){
  /* Get received character from ringbuffer
   * uart_getc() returns in the lower byte the received character and 
   * in the higher byte (bitmask) the last receive error
   * UART_NO_DATA is returned when no data is available.   */
  unsigned int c = uart_getc();
  
  if ( c & UART_NO_DATA ){
    // no data available from UART 
  }
  else {
    // new data available from UART check for Frame or Overrun error
    if ( c & UART_FRAME_ERROR ) {
      /* Framing Error detected, i.e no stop bit detected */
      uart_puts_P("UART Frame Error: ");
    }
    if ( c & UART_OVERRUN_ERROR ) {
      /* Overrun, a character already present in the UART UDR register was 
       * not read by the interrupt handler before the next character arrived,
       * one or more received characters have been dropped */
      uart_puts_P("UART Overrun Error: ");
    }
    if ( c & UART_BUFFER_OVERFLOW ) {
      /* We are not reading the receive buffer fast enough,
       * one or more received character have been dropped  */
      uart_puts_P("Buffer overflow error: ");
    }
    
    // Add char to input buffer
    data_in[data_count] = c;
    
    // Return is signal for end of command input
    if (data_in[data_count] == CHAR_RETURN) {
      // Reset to 0, ready to go again
      data_count = 0;
      uart_puts(RETURN_NEWLINE);
      
      copy_command();
      process_command();
      uart_ok();
    } 
    else {
      data_count++;
    }
    
    uart_putc( (unsigned char)c );
  }
}

int main(void) { 

  /*  Initialize UART library, pass baudrate and AVR cpu clock
   *  with the macro 
   *  UART_BAUD_SELECT() (normal speed mode )
   *  or 
   *  UART_BAUD_SELECT_DOUBLE_SPEED() ( double speed mode)  */
  uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) ); 
  
  // now enable interrupt, since UART library is interrupt controlled
  sei();
  
  /*  Transmit string to UART
   *  The string is buffered by the uart library in a circular buffer
   *  and one character at a time is transmitted to the UART using interrupts.
   *  uart_puts() blocks if it can not write the whole string to the circular 
   *  buffer */
  uart_puts("-----");
  uart_puts(RETURN_NEWLINE);
  uart_puts("USAGE:");
  uart_puts(RETURN_NEWLINE);
  uart_puts("SET PIN HIGH: SHn where 'n' is the pin number");
  uart_puts(RETURN_NEWLINE);
  uart_puts("SET PIN LOW: SLn where 'n' is the pin number");
  uart_puts(RETURN_NEWLINE);
  uart_puts("TOGGLE PIN: STn where 'n' is the pin number");
  uart_puts(RETURN_NEWLINE);


  uart_puts("INIT IO NOW");
  uart_puts(RETURN_NEWLINE);
  initPins();
  uart_puts("PINS INIT OK");
  uart_puts(RETURN_NEWLINE);

  //testPin();
  //uart_puts("TEST OK");
  //uart_puts(RETURN_NEWLINE);
     
  while (1) {
    process_uart();
  } 
}
