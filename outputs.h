#include <avr/io.h>

extern uint8_t buffer0;

extern void initPins(void);
extern void testPin(void);

extern void SetPWMOutput(char input);
extern void pinOn(int i);
extern void pinOff(int i);
extern void pinToggle(int i);
